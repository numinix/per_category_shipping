<?php
//
// +----------------------------------------------------------------------+
// |zen-cart Open Source E-commerce                                       |
// +----------------------------------------------------------------------+
// | Copyright (c) 2007-2008 Numinix http://www.numinix.com               |
// |																																			|
// | Portions Copyright (c) 2003-2008 The zen-cart developers             |
// |                                                                      |   
// | http://www.zen-cart.com/index.php                                    |   
// |                                                                      |   
// | Portions Copyright (c) 2003 osCommerce                               |
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.zen-cart.com/license/2_0.txt.                             |
// | If you did not receive a copy of the zen-cart license and are unable |
// | to obtain it through the world-wide-web, please send a note to       |
// | license@zen-cart.com so we can mail you a copy immediately.          |
// +----------------------------------------------------------------------+
// $Id: percategory.php 2 2008-06-06 07:24:54Z numinix $
//

define('MODULE_SHIPPING_PERCATEGORY_TEXT_TITLE', 'Per Category');
define('MODULE_SHIPPING_PERCATEGORY_TEXT_DESCRIPTION', 'Per Category Based Rates');
define('MODULE_SHIPPING_PERCATEGORY_TEXT_WAY', 'Shipping to');
define('MODULE_SHIPPING_PERCATEGORY_TEXT_UNITS', 'lb(s)');
define('MODULE_SHIPPING_PERCATEGORY_INVALID_ZONE', 'No shipping available to the selected country');
define('MODULE_SHIPPING_PERCATEGORY_UNDEFINED_RATE', 'The shipping rate cannot be determined at this time');