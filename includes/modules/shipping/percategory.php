<?php
/**
* @package shippingMethod
* @copyright Copyright 2007-2008 Numinix http://www.numinix.com
* @copyright Portions Copyright 2003-2008 Zen Cart Development Team
* @copyright Portions Copyright 2003 osCommerce
* @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
* @version $Id: percategory.php 11 2011-06-12 01:41:10Z numinix $
*/
/**
* Allows webmaster to set shipping rates based on zone, category, and one of number of units/weight/price
*
*/
class percategory extends base {
/**
* Enter description here...
*
* @var unknown_type
*/
	var $code;
/**
* Enter description here...
*
* @var unknown_type
*/
	var $title;
/**
* Enter description here...
*
* @var unknown_type
*/
	var $description;
/**
* Enter description here...
*
* @var unknown_type
*/
	var $icon;
/**
* Enter description here...
*
* @var unknown_type
*/
	var $enabled;
/**
* Enter description here...
*
* @return percategory
*/
	var $num_zones;
/**
* Enter description here...
*
* @return percategory
*/
	var $dest_zone;
/**
* Enter description here...
*
* @return percategory
*/
	function __construct() {
		global $order, $db;
		$geozones = $db->Execute("SELECT * FROM " . TABLE_GEO_ZONES);
		$this->num_zones = $geozones->RecordCount();
		$this->code = 'percategory';
		$this->title = defined('MODULE_SHIPPING_PERCATEGORY_TEXT_TITLE') ? MODULE_SHIPPING_PERCATEGORY_TEXT_TITLE : '';
		$this->description = defined('MODULE_SHIPPING_PERCATEGORY_TEXT_DESCRIPTION') ? MODULE_SHIPPING_PERCATEGORY_TEXT_DESCRIPTION : '';
		$this->sort_order = defined('MODULE_SHIPPING_PERCATEGORY_SORT_ORDER') ? MODULE_SHIPPING_PERCATEGORY_SORT_ORDER : '';
		$this->icon = '';
		$this->tax_class = defined('MODULE_SHIPPING_PERCATEGORY_TAX_CLASS') ? MODULE_SHIPPING_PERCATEGORY_TAX_CLASS : '';
		$this->tax_basis = defined('MODULE_SHIPPING_PERCATEGORY_TAX_BASIS') ? MODULE_SHIPPING_PERCATEGORY_TAX_BASIS : '';
		$this->groups = defined('MODULE_SHIPPING_PERCATEGORY_GROUPS') ? (int)MODULE_SHIPPING_PERCATEGORY_GROUPS : 0;
		$this->calculation_method = defined('MODULE_SHIPPING_PERCATEGORY_CALCULATION_METHOD') ? MODULE_SHIPPING_PERCATEGORY_CALCULATION_METHOD : '';
		
// disable only when entire cart is free shipping
		if ((zen_get_shipping_enabled($this->code))) {
			$this->enabled = ((MODULE_SHIPPING_PERCATEGORY_STATUS == 'True') ? true : false);
		}
		if ($this->enabled == true && IS_ADMIN_FLAG === false) {
			for ($i = 1; $i <= $this->num_zones; $i++) {
				if (defined('MODULE_SHIPPING_PERCATEGORY_ZONE_' . $i) && (int) constant('MODULE_SHIPPING_PERCATEGORY_ZONE_' . $i) > 0) {
					$check = $db->Execute("select zone_id from " . TABLE_ZONES_TO_GEO_ZONES . " where geo_zone_id = '" . constant('MODULE_SHIPPING_PERCATEGORY_ZONE_' . $i) . "' and (zone_country_id = '" . $order->delivery['country']['id'] . "' OR zone_country_id = 0) order by zone_id");
					while (!$check->EOF) {
						if ($check->fields['zone_id'] < 1) {
							$this->dest_zone = $i;
							break 2;
						}
						elseif ($check->fields['zone_id'] == $order->delivery['zone_id']) {
							$this->dest_zone = $i;
							break 2;
						}
						$check->MoveNext();
					} // end while
				} // END if ((int)constant('MODULE_SHIPPING_PERCATEGORY_ZONE_' . $i) > 0)
			} // END for ($i=1; $i<=$this->num_zones; $i++)
			if ($this->dest_zone < 1) {
				$this->enabled = false;
			}
		} // END if ($this->enabled == true)
	} // END function percategory()
/**
* @desc calculates the shipping rate to be used in the quote()
* @return $shipping
*/
	function calculation($products_array, $dest_zone, $groups) {
		global $order, $shipping_weight, $db;
		$shipping = 0;
		$order_totals_default = 0;
		for ($i = 1; $i <= $this->groups; $i++) {
			$group_array[$i] = explode(",", constant('MODULE_SHIPPING_PERCATEGORY_LIST_' . $i . '_' . $dest_zone));
			$order_totals[$i] = 0;
			$group[$i] = false;
		}
		for ($i = 0; $i < count($products_array); $i++) {
			$products_id = $products_array[$i]['id'];
			if ($products_array[$i]['quantity'] > 0) {
				$products_qty = $products_array[$i]['quantity'];
			}
			else {
// for external mods
				$products_qty = 1;
			}
// shipping adjustment
			switch (MODULE_SHIPPING_PERCATEGORY_MODE) {
			case ('price') : $order_total = $products_array[$i]['final_price'] * $products_qty;
				break;
			case ('weight') : $order_total = $products_array[$i]['weight'] * $products_qty;
				break;
			case ('item') : $order_total = $products_qty;
				break;
			}
// totals up each group of categories
			$match_found = false;
			for ($j = 1; $j <= $groups; $j++) {
				foreach ($group_array[$j] as $category_id) {
					if (zen_get_products_virtual($products_id) || zen_get_product_is_always_free_shipping($products_id)) {
// virtual or free shipping product, no need to return a rate
// here we set the match found to true in case there are other products in the cart
						$match_found = true;
						break 2;
					}
					if (zen_product_in_category($products_id, $category_id)) {
						$group[$j] = true;
						$order_totals[$j] += $order_total;
						$match_found = true;
						break 2; // doesn't matter if it's in another group
					}
				}
			} // END LOOP
			if (!$match_found) {
// add the total to the total for the default group
				$order_totals_default += $order_total;
// set not found globally in class
				$this->match_not_found = true;
			}
		} // END LOOP
// USES CATEGORY TOTALS TO FIND COST AMOUNT
		for ($j = 1; $j <= $groups; $j++) {
			if ($group[$j]) {
				$table_cost[$j] = $this->multi_explode(',', ':', constant('MODULE_SHIPPING_PERCATEGORY_COST_' . $j . '_' . $dest_zone));
				$size = sizeof($table_cost[$j]);
				for ($k = 0, $n = $size; $k < $n; $k += 2) {
					round($order_totals[$j], 9);
					if ($order_totals[$j] <= $table_cost[$j][$k]) {
						if ($table_cost[$j][$k + 1] < 0)
							return 'false'; // should only really be setup on the first
						if ($this->calculation_method == "sum") {
							$shipping += $table_cost[$j][$k + 1]; // ADD COST FROM GROUP $i TABLE
						}
						else {
							if (($table_cost[$j][$k + 1] > $shipping) || !$shipping) {
								$shipping = $table_cost[$j][$k + 1];
							}
						}
						break;
					}
				}
// if the measured amount is greater than the table, use maximal cost
				if ($order_totals[$j] > $table_cost[$j][$size - 2]) {
					$increment = $table_cost[$j][$size - 1] - $table_cost[$j][$size - 3];
					$quantity_remaining = $order_totals[$j] - $table_cost[$j][$size - 2];
					if ($this->calculation_method == "sum") {
						$shipping += $table_cost[$j][$size - 1] + ($increment * $quantity_remaining);
					}
					else {
						if ((($table_cost[$j][$size - 1] + ($increment * $quantity_remaining)) > $shipping) || !$shipping) {
							$shipping = $table_cost[$j][$size - 1] + ($increment * $quantity_remaining);
						}
					}
				}
			}
		} // END LOOP
		if ($this->match_not_found) {
// DEFAULT GROUP
			if (defined(constant('MODULE_SHIPPING_PERCATEGORY_DEFAULT_METHOD_' . $dest_zone)) && constant('MODULE_SHIPPING_PERCATEGORY_DEFAULT_METHOD_' . $dest_zone) == 'table') {
// use default table for zone
				$table_cost_default = $this->multi_explode(',', ':', constant('MODULE_SHIPPING_PERCATEGORY_DEFAULT_COST_' . $dest_zone));
				$size = sizeof($table_cost_default);
				for ($k = 0, $n = $size; $k < $n; $k += 2) {
					round($order_totals_default, 9);
					if ($order_totals_default <= $table_cost_default[$k]) {
						echo $table_cost_default[$k + 1];
						if ($table_cost_default[$k + 1] < 0)
							return 'false'; // should only really be setup on the first
						if ($this->calculation_method == "sum") {
							$shipping += $table_cost_default[$k + 1]; // ADD COST FROM GROUP $i TABLE
						}
						else {
							if (($table_cost_default[$k + 1] > $shipping) || !$shipping) {
								$shipping = $table_cost_default[$k + 1];
							}
						}
						break;
					}
				}
// if the measured amount is greater than the table, use maximal cost or increment
				if ($order_totals_default > $table_cost_default[$size - 2]) {
					$increment = $table_cost_default[$size - 1] - $table_cost_default[$size - 3];
					$quantity_remaining = $order_totals_default - $table_cost_default[$size - 2];
					if ($this->calculation_method == "sum") {
// calculate the increment
						$shipping += $table_cost_default[$size - 1] + ($increment * $quantity_remaining); // use increment
					}
					else {
						if ((($table_cost_default[$size - 1] + ($increment * $quantity_remaining)) > $shipping) || !$shipping) {
							$shipping = $table_cost_default[$size - 1] + $increment * $quantity_remaining;
						}
					}
				}
			}
			elseif (defined(constant('MODULE_SHIPPING_PERCATEGORY_DEFAULT_METHOD_' . $dest_zone)) && constant('MODULE_SHIPPING_PERCATEGORY_DEFAULT_METHOD_' . $dest_zone) == 'amazon') {
// use AMAZON method
				$base_amount = constant('MODULE_SHIPPING_PERCATEGORY_AMAZON_HANDLING_' . $dest_zone);
				$rate = constant('MODULE_SHIPPING_PERCATEGORY_AMAZON_RATE_' . $dest_zone);
				round($order_totals_default, 9);
				$item_shipping_amount = $base_amount + ($rate * $order_totals_default);
				if ($this->calculation_method == "sum") {
					$shipping += $item_shipping_amount;
				}
				else {
					if ($item_shipping_amount > $shipping)
						$shipping = $item_shipping_amount;
				}
			}
			else {
// disable the module
				$shipping = 'false';
			}
		}
		$debug = false;
		if ($debug) {
			for ($s = 1; $s <= $groups; $s++) {
				echo 'Group ' . $s . ':<br/>';
				print_r($group_array[$s]);
				echo '<br/>Cost: ';
				print_r($table_cost[$s]);
				echo '<br/>Order Total: ';
				print_r($order_totals[$s]);
				echo '<br/><br/>';
			}
			echo 'SHIPPING: ' . $shipping;
			die();
		}
		return $shipping;
	}
	function multi_explode($delim1, $delim2, $string) {
		$new_data = array();
		$data = explode($delim1, $string);
		foreach ($data as $key => $value) {
			$new_data = array_merge($new_data, explode($delim2, $value));
		}
		return $new_data;
	}
/**
* Enter description here...
*
* @param unknown_type $method
* @return unknown
*/
	function quote($method = '') {
		global $order, $shipping_weight, $shipping_num_boxes, $total_count, $db;
// disable for virtual and free shipping items
		if (($order->content_type == 'virtual') || ($_SESSION['cart']->free_shipping_items() == $_SESSION['cart']->count_contents()))
			return false;
// Check for category matches
		$shipping = $this->calculation($_SESSION['cart']->get_products(), $this->dest_zone, $this->groups);
		if ($shipping === 'false')
			return false;
		if (defined('MODULE_SHIPPING_PERCATEGORY_MODE') && MODULE_SHIPPING_PERCATEGORY_MODE == 'weight') {
			$shipping = $shipping * $shipping_num_boxes;
// show boxes if weight
			switch (SHIPPING_BOX_WEIGHT_DISPLAY) {
			case (0) : $show_box_weight = '';
				break;
			case (1) : $show_box_weight = ' (' . $shipping_num_boxes . ' ' . TEXT_SHIPPING_BOXES . ')';
				break;
			case (2) : $show_box_weight = ' (' . number_format($shipping_weight * $shipping_num_boxes, 2) . TEXT_SHIPPING_WEIGHT . ')';
				break;
				default :
					$show_box_weight = ' (' . $shipping_num_boxes . ' x ' . number_format($shipping_weight, 2) . TEXT_SHIPPING_WEIGHT . ')';
					break;
			}
		}
		$get_gzn = $db->Execute("select geo_zone_name from " . TABLE_GEO_ZONES . " where geo_zone_id = '" . constant('MODULE_SHIPPING_PERCATEGORY_ZONE_' . $this->dest_zone) . "' LIMIT 1");
		while (!$get_gzn->EOF) {
			$gzn = $get_gzn->fields['geo_zone_name'];
			$get_gzn->MoveNext();
		}
		$leadtime = constant('MODULE_SHIPPING_PERCATEGORY_LEADTIME_' . $this->dest_zone);
		if (!$leadtime) {
			if (!defined('MODULE_SHIPPING_PERCATEGORY_LEADTIME')) {
				$leadtime = MODULE_SHIPPING_PERCATEGORY_TEXT_WAY . ' ' . $gzn;
			}
			else {
				$leadtime = MODULE_SHIPPING_PERCATEGORY_LEADTIME;
			}
		}
		$this->quotes = array('id' => $this->code, 'module' => constant('MODULE_SHIPPING_PERCATEGORY_METHOD_NAME_' . $this->dest_zone) . $show_box_weight, 'info' => $this->info(), 'methods' => array(array('id' => $this->code, 'title' => $leadtime, 'cost' => $shipping + constant('MODULE_SHIPPING_PERCATEGORY_HANDLING_' . $this->dest_zone))));
		if ($this->tax_class > 0) {
			$this->quotes['tax'] = zen_get_tax_rate($this->tax_class, $order->delivery['country']['id'], $order->delivery['zone_id']);
		}
		if (zen_not_null($this->icon))
			$this->quotes['icon'] = zen_image($this->icon, $this->title);
		return $this->quotes;
	}
// method added for expanded info in FEAC
	function info() {
		return MODULE_SHIPPING_PERCATEGORY_INFO; // add a description here or leave blank to disable
	}
/**
* Enter description here...
*
* @return unknown
*/
	function check() {
		global $db;
		if (!isset($this->_check)) {
			$check_query = $db->Execute("select configuration_value from " . TABLE_CONFIGURATION . " where configuration_key = 'MODULE_SHIPPING_PERCATEGORY_STATUS'");
			$this->_check = $check_query->RecordCount();
		}
		return $this->_check;
	}
/**
* Enter description here...
*
*/
	function install() {
		global $db;
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Enable Table Method', 'MODULE_SHIPPING_PERCATEGORY_STATUS', 'True', 'Do you want to offer percategory rate shipping?', '6', '0', 'zen_cfg_select_option(array(\'True\', \'False\'), ', now())");
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Table Method', 'MODULE_SHIPPING_PERCATEGORY_MODE', 'weight', 'The shipping cost is based on the order total, the total weight of the items ordered, or the total number of items orderd.', '6', '0', 'zen_cfg_select_option(array(\'weight\', \'price\', \'item\'), ', now())");
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, use_function, set_function, date_added) values ('Tax Class', 'MODULE_SHIPPING_PERCATEGORY_TAX_CLASS', '0', 'Use the following tax class on the shipping fee.', '6', '0', 'zen_get_tax_class_title', 'zen_cfg_pull_down_tax_classes(', now())");
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Tax Basis', 'MODULE_SHIPPING_PERCATEGORY_TAX_BASIS', 'Shipping', 'On what basis is Shipping Tax calculated. Options are<br />Shipping - Based on customers Shipping Address<br />Billing Based on customers Billing address<br />Store - Based on Store address if Billing/Shipping Zone equals Store zone', '6', '0', 'zen_cfg_select_option(array(\'Shipping\', \'Billing\', \'Store\'), ', now())");
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Sort Order', 'MODULE_SHIPPING_PERCATEGORY_SORT_ORDER', '0', 'Sort order of display.', '6', '0', now())");
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Calculation Method', 'MODULE_SHIPPING_PERCATEGORY_CALCULATION_METHOD', 'sum', 'Return rates as sum of products or maximal rate?', '6', '0', 'zen_cfg_select_option(array(\'sum\', \'max\'), ', now())");
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Shipping Info', 'MODULE_SHIPPING_PERCATEGORY_INFO', '', 'Add a description that will display in Fast and Easy AJAX Checkout', '6', '0', 'zen_cfg_textarea(', now())");
		for ($i = 1; $i <= $this->num_zones; $i++) {
			$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, use_function, set_function, date_added) values ('Zone " . $i . " - Shipping Zone', 'MODULE_SHIPPING_PERCATEGORY_ZONE_" . $i . "', '0', 'Select a zone. If set to \"none\", this table will be skipped.', '6', '0', 'zen_get_zone_class_title', 'zen_cfg_pull_down_zone_classes(', now())");
			$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Zone " . $i . " - Handling Fee', 'MODULE_SHIPPING_PERCATEGORY_HANDLING_" . $i . "', '0', 'Handling fee for this shipping method.', '6', '0', now())");
			$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('DEFAULT Method', 'MODULE_SHIPPING_PERCATEGORY_DEFAULT_METHOD_" . $i . "', 'table', 'Use the default table or amazon style calculator for products not in a category group?', '6', '0', 'zen_cfg_select_option(array(\'table\', \'amazon\', \'disable\'), ', now())");
			$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Zone " . $i . " - DEFAULT Shipping Table', 'MODULE_SHIPPING_PERCATEGORY_DEFAULT_COST_" . $i . "', '25:8.50,50:5.50,10000:0.00', 'Use this table rate for all categories that are not defined. Example: 1:3,2:5,3:7,etc.. For up to 1 charge $3, for up to 2 charge $5, for up to 3 charge $7, etc', '6', '0', 'zen_cfg_textarea(', now())");
			$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Zone " . $i . " - Amazon Handling Fee', 'MODULE_SHIPPING_PERCATEGORY_AMAZON_HANDLING_" . $i . "', '0', 'Add a handling fee for the order (adds with other handling fee)', '6', '0', now())");
			$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Zone " . $i . " - Amazon Rate', 'MODULE_SHIPPING_PERCATEGORY_AMAZON_RATE_" . $i . "', '0', 'Cost per unit based on the TABLE METHOD defined above', '6', '0', now())");
			$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Zone " . $i . " - Method Name', 'MODULE_SHIPPING_PERCATEGORY_METHOD_NAME_" . $i . "', 'Ground', 'Enter the shipping method name for this zone', '6', '0', now())");
			$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Zone " . $i . " - Leadtime', 'MODULE_SHIPPING_PERCATEGORY_LEADTIME_" . $i . "', '3-5 Days', 'Enter the shipping time frame for this zone', '6', '0', now())");
			for ($j = 1; $j <= $this->groups; $j++) {
				$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Zone " . $i . " - Group " . $j . " - Category List', 'MODULE_SHIPPING_PERCATEGORY_LIST_" . $j . "_" . $i . "', '1,2,3,4', 'Enter the category numbers included for the rate table', '6', '0', 'zen_cfg_textarea(', now())");
				$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Zone " . $i . " - Group " . $j . " - Shipping Table', 'MODULE_SHIPPING_PERCATEGORY_COST_" . $j . "_" . $i . "', '25:8.50,50:5.50,10000:0.00', 'The shipping cost is based on the total cost or weight of items or count of the items. Example: 1:3,2:5,3:7,etc.. For up to 1 charge $3, for up to 2 charge $5, for up to 3 charge $7, etc', '6', '0', 'zen_cfg_textarea(', now())");
			}
		}
	}
/**
* Enter description here...
*
*/
	function remove() {
		global $db;
		$db->Execute("delete from " . TABLE_CONFIGURATION . " where configuration_key in ('" . implode("', '", $this->keys()) . "')");
	}
/**
* Enter description here...
*
* @return unknown
*/
	function keys() {
		$keys = array('MODULE_SHIPPING_PERCATEGORY_STATUS', 'MODULE_SHIPPING_PERCATEGORY_MODE', 'MODULE_SHIPPING_PERCATEGORY_TAX_CLASS', 'MODULE_SHIPPING_PERCATEGORY_TAX_BASIS', 'MODULE_SHIPPING_PERCATEGORY_SORT_ORDER', 'MODULE_SHIPPING_PERCATEGORY_CALCULATION_METHOD', 'MODULE_SHIPPING_PERCATEGORY_INFO');
		for ($i = 1; $i <= $this->num_zones; $i++) {
			$keys[] = 'MODULE_SHIPPING_PERCATEGORY_ZONE_' . $i;
			$keys[] = 'MODULE_SHIPPING_PERCATEGORY_HANDLING_' . $i;
			$keys[] = 'MODULE_SHIPPING_PERCATEGORY_DEFAULT_METHOD_' . $i;
			$keys[] = 'MODULE_SHIPPING_PERCATEGORY_AMAZON_HANDLING_' . $i;
			$keys[] = 'MODULE_SHIPPING_PERCATEGORY_AMAZON_RATE_' . $i;
			$keys[] = 'MODULE_SHIPPING_PERCATEGORY_DEFAULT_COST_' . $i;
			$keys[] = 'MODULE_SHIPPING_PERCATEGORY_METHOD_NAME_' . $i;
			$keys[] = 'MODULE_SHIPPING_PERCATEGORY_LEADTIME_' . $i;
			for ($j = 1; $j <= $this->groups; $j++) {
				if (defined('MODULE_SHIPPING_PERCATEGORY_LIST_' . $j . '_' . $i)) {
					$keys[] = 'MODULE_SHIPPING_PERCATEGORY_LIST_' . $j . '_' . $i;
				}
				if (defined('MODULE_SHIPPING_PERCATEGORY_COST_' . $j . '_' . $i)) {
					$keys[] = 'MODULE_SHIPPING_PERCATEGORY_COST_' . $j . '_' . $i;
				}
			}
		}
		return $keys;
	}
}